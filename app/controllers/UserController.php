<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
//        return "Hello";
		return View::make('users.user_homepage');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return 'HELLO';
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function login()
	{
		$input = Input::all();
		return $input;
		return View::make('users.user_dashboard');
		return "Hello";
	}

    //redirect to the login otherwise redirect to register

    public function postAuth(){

        if(Input::get('login')){

            $validator = Validator::make(Input::all(), User::$login_rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return View::make('users.user_homepage')
                    ->withErrors($validator) // send back all errors to the login form
                    ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
            }else{
                // create our user data for the authentication
                $user_data = array(
                    'email'     => Input::get('email'),
                    'password'  => Input::get('password')
                );
//                return $user_data['password'];

                if (Auth::attempt($user_data)) {
//                    return Auth::user();
                    //redirect the user to user settings page
                    return View::make('users.user_settings')->with('user_data', Auth::user());

                }else{
                    return View::make('users.user_homepage')->with('login_failed_message', 'Login Failed: User not found');
                }

            }

        }else if(Input::get('register')){
            return View::make('users.user_registration');
        }
    }

//    fetch all the information of user registration
    public function register(){
        $validate = Validator::make(Input::all(), User::$registration_rules);
        if($validate->passes()){
            User::create([
                'firstname'=> Input::get('firstname'),
                'lastname'=>  Input::get('lastname'),
                'email'=>     Input::get('email'),
                'password'=>  Hash::make(Input::get('password')),
                'phone'=>     Input::get('phone'),
                'cell_phone'=>Input::get('cell_phone'),
                'status'=>    0,//Input::has('age') ? intval(Input::get('age')) : null,
            ]);

            return Redirect::to('user')->with('success_message', 'Please wait for the review of your application');
        }else{
            return View::make('users.user_registration')
                ->withErrors($validate->messages())
                ->withInput(Input::except('password', 'password_confirmation'));
        }


    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function settings()
	{
		$input = Input::all();
//		return Input::get('email');
		return View::make('users.user_settings');
		return "Hello";
	}


}
