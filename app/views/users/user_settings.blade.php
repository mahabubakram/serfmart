/**
* Created by PhpStorm.
* User: MahabubAkram
* Date: 1/18/2015
* Time: 3:47 PM
*/

@extends('layouts.product')
@section('main')


    <!-- start: Create Product Form -->
    <h2 style="font-size:1.5em;">Vendor Information</h2>
    <hr>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-2">
            {{ Form::open(array( 'route' =>array('user.login'), 'method' => 'POST')) }}
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>*Vendor First Name</label>
                    <input type="text" name="firstname" value="{{$user_data->firstname}}" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address." readonly>
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>

            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>*Vendor Last Name</label>
                    <input type="text" name="lastname" value="{{$user_data->lastname}}" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address." readonly>
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>

            <div class="row control-group">
                <div class="form-group col-xs-4 floating-label-form-group controls">
                    <label>*Status</label>
                    <select name="category" class="form-control" disabled>
                        <option value="one" @if ($user_data->status == 2) selected has-error @endif>Active</option>
                        <option value="two" @if ($user_data->status == 1) selected has-error @endif>Hidden</option>
                        <option value="three" @if ($user_data->status == 0) selected has-error @endif>Disable</option>
                    </select>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
        </div>
    </div>


    <h2 style="font-size:1.5em;">Contact Information</h2>
    <hr>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-2">
            {{ Form::open(array( 'route' =>array('user.login'), 'method' => 'POST')) }}
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Email</label>
                    <input type="email" name="email" value="{{$user_data->email}}" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Phone</label>
                    <input type="text" name="phone" value="{{$user_data->phone}}" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Mobile</label>
                    <input type="text" name="cell_phone" value="{{$user_data->cell_phone}}" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
        </div>
    </div>

    <h2 style="font-size:1.5em;">Shipping Information</h2>
    <hr>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-2">
            {{ Form::open(array( 'route' =>array('user.login'), 'method' => 'POST')) }}
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Address</label>
                    <input type="email" name="email" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>City</label>
                    <input type="email" name="email" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Post Code</label>
                    <input type="email" name="email" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-10" style="font-size: 1em;">
                    <button type="submit" style="font-size: 1.5em; float: right" class="fa fa-plus btn btn-success btn-lg"> Submit</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>

@stop