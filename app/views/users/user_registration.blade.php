/**
* Created by PhpStorm.
* User: MahabubAkram
* Date: 1/18/2015
* Time: 3:47 PM
*/

@extends('layouts.product')
@section('main')


    <!-- start: Create Product Form -->
    <h2 style="font-size:2em;">User Information</h2>
    <hr>
    <div class="help-block text-danger">
        @if($errors->count() > 0)
            <p>The following errors have occurred:</p>

            <ul>
                @foreach($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        @endif
    </div>


    <div class="row">
        <div class="col-lg-6 col-lg-offset-1">
            {{ Form::open(array( 'route' =>array('user.register'), 'method' => 'POST')) }}
            <h2 style="font-size:1em; margin-bottom: 2%; color:#463C54;">Name Input Section</h2>
            <div class="row control-group @if ($errors->has('firstname')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>First Name</label>
                    <input type="text" name="firstname"  value="{{Input::get('firstname')}}" class="form-control" placeholder="First Name" id="firstname" required data-validation-required-message="Please enter your first name.">
                    @if ($errors->has('firstname')) <p class="help-block text-danger">{{ $errors->first('firstname') }}</p> @endif
                </div>
            </div>
            <div class="row control-group @if ($errors->has('lastname')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>Last Name</label>
                    <input type="text" name="lastname" value="{{Input::get('lastname')}}" class="form-control" placeholder="Last Name" id="lastname" required data-validation-required-message="Please enter your last name.">
                    @if ($errors->has('lastname')) <p class="help-block text-danger">{{ $errors->first('lastname') }}</p> @endif
                </div>
            </div>

            <h2 style="font-size:1em; margin-top: 5%; margin-bottom:2%; color:#463C54;">Email Input Section</h2>
            <div class="row control-group @if ($errors->has('email')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>Email Address</label>
                    <input type="email" name="email" value="{{Input::get('email')}}" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                    @if ($errors->has('email')) <p class="help-block text-danger">{{ $errors->first('email') }}</p> @endif
                </div>
            </div>

            <div class="row control-group @if ($errors->has('email_confirmation')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>Confirm Email Address</label>
                    <input type="email" name="email_confirmation" value="{{Input::get('email_confirmation')}}" class="form-control" placeholder="Confirm Email Address" id="email" required data-validation-required-message="Please enter the same email address.">
                    @if ($errors->has('email_confirmation')) <p class="help-block text-danger">{{ $errors->first('email_confirmation') }}</p> @endif
                </div>
            </div>

            <h2 style="font-size:1em; margin-top: 5%; margin-bottom:2%; color:#463C54;">Password Input Section</h2>
            <div class="row control-group @if ($errors->has('password')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" id="password" required data-validation-required-message="Please enter your password.">
                    @if ($errors->has('password')) <p class="help-block text-danger">{{ $errors->first('password') }}</p> @endif
                </div>
            </div>

            <div class="row control-group @if ($errors->has('password_confirmation')) has-error @endif">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label style="color: red">*</label>
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="password" required data-validation-required-message="Please enter the same password again.">
                    @if ($errors->has('password_confirmation')) <p class="help-block text-danger">{{ $errors->first('password_confirmation') }}</p> @endif
                </div>
            </div>

            <h2 style="font-size:1em; margin-top: 5%; margin-bottom:2%; color:#463C54;">Optional Input Section</h2>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Phone</label>
                    <input type="text" name="phone" value="{{Input::get('phone')}}" class="form-control" placeholder="+880......" id="phone">
                    <p class="help-block text-danger"></p>
                </div>
            </div>

            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Cell Phone</label>
                    <input type="text" name="cell_phone" value="{{ Input::get('cell_phone') }}" class="form-control" placeholder="+880......" id="cell_phone">
                    <p class="help-block text-danger"></p>
                </div>
            </div>


            <div id="success"></div>
            <div class="row" style="margin-top: 5%;">
                <div class="form-group col-xs-12" style="font-size: 1em;">
                    <button type="submit" style="font-size: 1.5em;" class="fa fa-sign-in btn btn-success btn-group-sm"> Sign Up</button>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>


@stop