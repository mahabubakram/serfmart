/**
* Created by PhpStorm.
* User: MahabubAkram
* Date: 1/18/2015
* Time: 3:47 PM
*/

@extends('layouts.product')
@section('main')


    <div class="col-sm-9 col-md-9 main">
        <!--         <h1 class="page-header">Dashboard</h1>-->
        <div class="row placeholders">
            <div class="col-md-3 panel panel-default">
                <div class="panel-heading" align="center">
                    <h3 class="panel-title">Categories</h3>
                </div>
                <div class="panel-body" align="center">
                    52
                </div>
            </div>
            <div class="col-md-offset-1 col-md-2 panel panel-default">
                <div class="panel-heading" align="center">
                    <h3 class="panel-title">Items</h3>
                </div>
                <div class="panel-body" align="center">
                    20
                </div>
            </div>
            <div class="col-md-offset-1 col-md-2 panel panel-default">
                <div class="panel-heading" align="center">
                    <h3 class="panel-title">Amount</h3>
                </div>
                <div class="panel-body" align="center">
                    20,0000
                </div>
            </div>
            <div class="col-md-offset-1 col-md-2 panel panel-default">
                <div class="panel-heading" align="center">
                    <h3 class="panel-title">Reviews</h3>
                </div>
                <div class="panel-body" align="center">
                    10
                </div>
            </div>
        </div>

        <!--End of panels-->
        <div class="row user-info">
            <h4 style="margin-left: 2%;">Vendor Information</h4>

            <div class="row">
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Name:</div>
                    <div class="user-value">Amarshop</div>
                </div>
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Status:</div>
                    <div class="user-value">Active</div>
                </div>
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Email:</div>
                    <div class="user-value">abd@gmail.com</div>
                </div>
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Phone:</div>
                    <div class="user-value">+880258369:</div>
                </div>
             </div>

            <div class="row">
                <div class='form-group'>
                    <div class="col-md-2" style="float: right; padding-right: 5px; padding-bottom: 5px; margin-right: 5%; margin-bottom: 1%; text-align: center;">
                        <a href="{{action('UserController@settings');}}" class="btn btn-primary bt-lg">Edit</a>
                    </div>
                </div>
            </div>
        </div>


        <!-- End of General Information-->

        <div class="row user-info" style="margin-top: 4em;">
            <h4 style="margin-left: 2%;">Shipping Information</h4>

            <div class="row">
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Address:</div>
                    <div class="user-value">23/1 Mirpur</div>
                </div>
                <div class='form-group' style="clear: both;">
                    <div class="user-key">City:</div>
                    <div class="user-value">Dhaka</div>
                </div>
                <div class='form-group' style="clear: both;">
                    <div class="user-key">Zip Code:</div>
                    <div class="user-value">1207</div>
                </div>
            </div>

            <div class="row">
                <div class='form-group'>
                    <div class="col-md-2 btn btn-primary" style="float: right; padding-right: 5px; padding-bottom: 5px; margin-right: 5%; margin-bottom: 1%; text-align: center;">
                        {{ link_to_route('user.index', 'Edit') }}
                    </div>
                </div>
            </div>
        </div>

    </div>




    <div class="col-sm-3 col-md-3 sidebar">
        <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="#browse"> <span class="glyphicon glyphicon-zoom-in"></span> Browse</a></li>
            <li><a href="#create"> <span class="glyphicon glyphicon-star"></span> Create</a></li>
            <li><a href="#file-convert"> <span class="glyphicon glyphicon-certificate"></span> Match Key</a></li>
            <li><a href="#import"> <span class="glyphicon glyphicon-download-alt"></span> Import token</a></li>
            <li><a href="#feedback"> <span class="glyphicon glyphicon-thumbs-up"></span> Feedback</a></li>
        </ul>

        <ul class="nav nav-sidebar">
            <li><a href="#designer"> <span class="glyphicon glyphicon-user"></span> Designer</a></li>
            <li><a href="#avatar"> <span class="glyphicon glyphicon-user"></span> Avatar</a></li>
            <li><a href="#keyword"> <span class="glyphicon glyphicon-screenshot"></span> Keyword</a></li>
        </ul>
    </div>


@stop