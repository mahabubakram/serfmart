/**
 * Created by PhpStorm.
 * User: MahabubAkram
 * Date: 1/18/2015
 * Time: 3:47 PM
 */

@extends('layouts.user')
@section('main')


    <!-- start: Login Form -->
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            {{ Form::open(array( 'route' =>array('user.postAuth'), 'method' => 'POST')) }}
                <div class="row control-group">

                    <div class="help-block text-danger" >
                        @if(isset($login_failed_message))
                            <p>{{$login_failed_message}}</p>
                        @endif
                    </div>

                    @if(isset($success_message))
                        <p class="text-success" style="color: darkgreen; font-family: Arial;">{{$success_message}}</p>
                    @endif

                    <p> Fill the form with your login information.</p>
                    <div class="form-group col-xs-12 floating-label-form-group controls @if ($errors->has('email')) has-error @endif">
                        <label>Email Address</label>
                        <input type="email" name="email" value="{{Input::get('email')}}" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                        {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                        @if ($errors->has('email')) <p class="help-block text-danger">{{ $errors->first('email') }} </p> @endif
                    </div>
                </div>
                <div class="row control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls @if ($errors->has('email')) has-error @endif">
                        <label>Password</label>
                        <input type="password" rows="5" name="password" class="form-control" placeholder="Password" id="message" required data-validation-required-message="Please enter a password."/>
                        @if ($errors->has('password')) <p class="help-block text-danger">{{ $errors->first('password') }}</p> @endif

                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <button type="submit" class="btn btn-success btn-lg" name="login" value="login">Submit</button>
                    </div>
                </div>
            {{Form::close()}}

        </div>

        <div class="col-md-4 col-md-offset-1">

            {{ Form::open(array( 'route' =>array('user.postAuth'))) }}
            <div class="row control-group">
                <p> Not yet registered! Please register as an User.</p>
            </div>
            <br>
            <div id="success"></div>
            <div class="row">
                <div class="form-group col-xs-12">
                    <button type="submit" class="btn btn-success btn-lg" name="register" value="register">Register</button>
                </div>
            </div>
            {{Form::close()}}

        </div>


    </div>


@stop