/**
* Created by PhpStorm.
* User: MahabubAkram
* Date: 1/18/2015
* Time: 3:47 PM
*/

@extends('layouts.product')
@section('main')


    <!-- start: Create Product Form -->
    <h2 style="font-size:2em;">Product Information</h2>
    <hr>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-2">
            {{ Form::open(array( 'route' =>array('user.login'), 'method' => 'POST')) }}
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>*Product Name</label>
                    <input type="email" name="email" class="form-control" placeholder="Name of the product" id="email" required data-validation-required-message="Please enter your email address.">
                    {{--{{ Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email Address')) }}--}}
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-4 floating-label-form-group controls">
                    <label>*Categories</label>
                    <select name="category" class="form-control">
                        <option value="one">One</option>
                        <option value="two">Two</option>
                        <option value="three">Three</option>
                        <option value="four">Four</option>
                        <option value="five">Five</option>
                    </select>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-6 floating-label-form-group controls">
                    <label>*Sale Price</label>
                    <span class="form-inline">
                    <input type="text" name="sale-price" class="form-control" placeholder="Sale Price" id="sale-price" required data-validation-required-message="Please enter your product sale price.">
                    .BDT</span>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-6 floating-label-form-group controls">
                    <label>*Market Price</label>
                    <span class="form-inline">
                    <input type="text" name="market-price" class="form-control" value="0.00" placeholder="Market Price" id="market-price" required data-validation-required-message="Please enter your least product market price.">
                    .BDT</span>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-4 floating-label-form-group controls">
                    <label>*Quantity</label>
                    <input type="number" min="1" name="quantity" class="form-control" value="1" placeholder="Quantity" id="quantity" required data-validation-required-message="Please enter your product quantity.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Description</label>
                    <textarea type="text" name="description" class="form-control" placeholder="Description" id="description"></textarea>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-4 floating-label-form-group controls">
                    <label>*Image</label>
                    <span class="btn btn-default btn-file">
                        <input type="file">
                    </span>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Shipping Info</label>
                    <input type="text" name="shipping-info" class="form-control" placeholder="Shipping Info" id="shipping-info" required data-validation-required-message="Please enter your shipping info.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-10 floating-label-form-group controls">
                    <label>Payment Info</label>
                    <input type="text" name="payment-info" class="form-control" placeholder="Payment Info" id="payment-info" required data-validation-required-message="Please enter your payment info.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <br>

            <h2 style="font-size:1.5em;">Optional Product Information</h2>
            <hr>
            <div class="row control-group">
                <div class="form-group col-xs-20 floating-label-form-group controls">
                    <label>Product Status</label>
                    <input type="radio" name="payment-info">
                    <span>Active</span>
                    <input style="margin-left:7%;" type="radio" name="payment-info">
                    <span>Hidden</span>
                    <input style="margin-left: 7%;" type="radio" name="payment-info">
                    <span>Disable</span>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div id="success"></div>
            <div class="row">
                <div class="form-group col-xs-12" style="font-size: 1em;">
                    <button type="submit" style="font-size: 1.5em;" class="fa fa-plus btn btn-success btn-lg"> Submit</button>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>


@stop