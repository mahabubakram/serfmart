<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

//ProductController
Route::get('/createProduct', 'ProductController@create');
Route::resource('product','ProductController');
Route::controller('product', 'ProductController');

//UserController
Route::get('user', 'UserController@index');
Route::post('login', array('as'=>'user.login', 'uses'=>'UserController@login'));
Route::post('postAuth', array('as'=>'user.postAuth', 'uses'=>'UserController@postAuth'));
Route::post('register', array('as'=>'user.register', 'uses'=>'UserController@register'));
Route::get('/user/settings', array('as'=>'user.settings', 'uses'=>'UserController@settings'));
Route::get('logout', array('as'=>'user.logout', 'uses'=>'UserController@doLogout'));
Route::resource('user','UserController');
Route::controller('user', 'UserController');


//TestController
Route::get('/test', 'TestController@index');


