<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, HasRole;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    protected $fillable = array('firstname', 'lastname', 'email', 'password', 'address', 'city', 'phone',
                                'cell_phone', 'status', 'email', 'password');

    public static $registration_rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|email|Confirmed',
        'email_confirmation' => 'required|email',
        'password' => 'required|min:8|Confirmed',
        'password_confirmation' => 'required|min:8',
    ];

    public static $login_rules = array(
    'email'    => 'required|email', // make sure the email is an actual email
    'password' => 'required|min:8' // password can only be alphanumeric and has to be greater than 3 characters
    );


    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}
