<?php
/**
 * Created by PhpStorm.
 * User: MahabubAkram
 * Date: 1/18/2015
 * Time: 12:22 PM
 */

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * Ardent validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required|between:4,255'
    );


}