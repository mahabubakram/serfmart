<?php

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('12345678');
        $user = User::create(array(
                'email' => 'abcd@gmail.com',
                'password' => $password
            )
        );

    }

}
